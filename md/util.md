# hosc-util

## nrt-to-sexp

Read a
[SuperCollider](http://audiosynth.com/)
Nrt (non-realtime) file and print it as an
[s-expression](https://wiki.c2.com/?EssExpressions).

There is a simple heuristic that prints text as either a string or a symbol.

~~~~
$ hosc-util nrt-to-sexp ~/uc/the-center-is-between-us/salt/osc/fcd/c21.YZ.osc
((0.0 (/d_recv #u8(83 67 103 102 0 0 0 0 0 1 11 115 109 112 108 114 45 103 108...))
      (/g_new 1 0 0)
      (/b_allocRead 0 "/home/rohan/data/audio/instr/farfisa/aad/flute8.flac" ...) ...)
 (0.0 (/s_new smplr-gliss -1 0 1 duration 25.3429069519 sustain 26.0929069519...) ...)
 ...)
$
~~~~

# trace

Start a Udp server to read incoming Osc packets at the indicated port
number and print Ascii text representations.
