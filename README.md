hosc-util
---------

utilities (haskell modules and cli) related to [hosc](http://rohandrape.net/?t=hosc)

## cli

[util](?t=hosc-util&e=md/util.md)

© [rd](http://rohandrape.net/), 2011-2025, [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 66  Tried: 66  Errors: 0  Failures: 0
$
```
