all:
	echo "hosc-util"

clean:
	rm -f -R dist dist-newstyle
	(cd cmd ; make clean)

mk-cmd:
	(cd cmd ; make all install)

push-all:
	r.gitlab-push.sh hosc-util

push-tags:
	r.gitlab-push.sh hosc-util --tags

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound

print-exec:
	hsc3-setup cabal print-exec hosc- cmd/*.hs
