import Control.Monad {- base -}
import Data.List {- base -}
import Data.List.Split {- split -}
import Data.Word {- base -}
import Text.Printf {- base -}

import qualified Data.ByteString as ByteString {- bytestring -}
import qualified Data.ByteString.Lazy as ByteString.Lazy {- bytestring -}
import qualified Language.Scheme.Types as Scheme {- husk-scheme (3.19.4) -}
import qualified Network.Socket.ByteString as Network {- network -}

import qualified Music.Theory.Opt as Opt {- hmt-base -}

import Sound.Osc {- hosc -}
import qualified Sound.Osc.Fd as Osc.Fd {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Fd.Udp {- hosc -}

import qualified Sound.Osc.Packet.Pp as Pp {- hosc-util -}

import Sound.Sc3 {- hsc3 -}

-- * Nrt->Sexp

{- | If text is short (less than 32 characters) and is a legal
identifier (not properly checked) encode as a symbol, else as a string.
-}
sym_or_str :: String -> Scheme.LispVal
sym_or_str txt =
  if length txt < 32 && all (flip notElem " \t\n#") txt
    then Scheme.Atom txt
    else Scheme.String txt

{- | Convert Osc 'Datum' to Lisp value given text heuristic.

>>> let f = map (datum_to_lisp sym_or_str)
>>> f [Int32 0,Int64 0,Float 0,Double 0,TimeStamp 0]
[0,0,0.0,0.0,0.0]

>>> f [AsciiString (ascii "str"),Blob (blob_pack [0,1,2]),Midi (MidiData 0 1 2 3)]
[str,#u8(0 1 2),#u8(0 1 2 3)]
-}
datum_to_lisp :: (String -> Scheme.LispVal) -> Datum -> Scheme.LispVal
datum_to_lisp str d =
  case d of
    Int32 x -> Scheme.Number (fromIntegral x)
    Int64 x -> Scheme.Number (fromIntegral x)
    Float x -> Scheme.Float (realToFrac x)
    Double x -> Scheme.Float x
    AsciiString x -> str (ascii_to_string x)
    Blob x -> Scheme.ByteVector (ByteString.Lazy.toStrict x)
    TimeStamp x -> Scheme.Float x
    Midi (MidiData m1 m2 m3 m4) -> Scheme.ByteVector (ByteString.Lazy.toStrict (blob_pack [m1, m2, m3, m4]))

{- | Convert Osc 'Message' to Lisp value.
     If /sym/ then use symbol heuristic, else write strings.
     If /ty/ print type tags else don't.

>>> message_to_lisp (True,False) (Message "/c_set" [Int32 0,Float 1])
(/c_set 0 1.0)

>>> message_to_lisp (True,True) (Message "/c_set" [Int32 0,Float 1])
(/c_set ,if 0 1.0)
-}
message_to_lisp :: (Bool, Bool) -> Message -> Scheme.LispVal
message_to_lisp (sym, ty) (Message a d) =
  let mk = if sym then sym_or_str else Scheme.String
  in if ty
      then Scheme.List (mk a : mk (',' : map datum_tag d) : map (datum_to_lisp mk) d)
      else Scheme.List (mk a : map (datum_to_lisp mk) d)

{- | Convert Osc 'Bundle' to Lisp value.

>>> bundle_to_lisp (True,False) (Bundle 0 [Message "/c_set" [Int32 0,Float 1]])
(0.0 (/c_set 0 1.0))
-}
bundle_to_lisp :: (Bool, Bool) -> Bundle -> Scheme.LispVal
bundle_to_lisp opt (Bundle t m) = Scheme.List (Scheme.Float t : map (message_to_lisp opt) m)

{- | Convert Sc3 'Nrt' to Lisp value.

> let fn = "/home/rohan/uc/the-center-is-between-us/salt/osc/fcd/c21.YZ.osc"
> nrt_to_sexp_wr (True,False) fn
-}
nrt_to_sexp :: (Bool, Bool) -> Nrt -> Scheme.LispVal
nrt_to_sexp opt r =
  case nrt_non_ascending r of
    [] -> Scheme.List (map (bundle_to_lisp opt) (nrt_bundles r))
    _ -> error "nrt_to_sexp: non-ascending"

nrt_to_sexp_wr :: (Bool, Bool) -> FilePath -> IO ()
nrt_to_sexp_wr opt fn = do
  r <- readNrt fn
  putStrLn (show (nrt_to_sexp opt r))

-- * Trace

udp_recv_bytes :: Fd.Udp.Udp -> IO ByteString.ByteString
udp_recv_bytes = flip Network.recv 8192 . Fd.Udp.udpSocket

u8_to_char :: Word8 -> Char
u8_to_char = toEnum . fromIntegral

print_u8 :: Word8 -> String
print_u8 x = printf "%02x (%c)" x (u8_to_char x)

bytes_pp :: ByteString.ByteString -> String
bytes_pp =
  let f = intercalate "  " . map print_u8
  in unlines . map f . chunksOf 4 . ByteString.unpack

packet_pp :: ByteString.ByteString -> String
packet_pp = Pp.packetPp (Just 5) . decodePacket_strict

osc_trace_str :: Bool -> ByteString.ByteString -> String
osc_trace_str verbose b =
  if verbose
    then unlines [bytes_pp b, packet_pp b]
    else packet_pp b

osc_trace :: Int -> Bool -> IO ()
osc_trace p verbose = do
  let pr b = putStrLn (osc_trace_str verbose b)
      fn fd = forever (udp_recv_bytes fd >>= pr)
  Osc.Fd.withTransport (Fd.Udp.udp_server p) fn

-- * Main

usage :: [String]
usage =
  [ "hosc-util"
  , ""
  , "\tnrt-to-sexp nrt-file..."
  , "\ttrace port:int"
  ]

main :: IO ()
main = do
  (o, a) <- Opt.opt_get_arg True usage [("verbose", "True", "Bool", "Be verbose")]
  case a of
    "nrt-to-sexp" : fn -> mapM_ (nrt_to_sexp_wr (True, False)) fn
    ["trace", n] -> osc_trace (read n) (Opt.opt_read o "verbose")
    _ -> putStrLn (unlines usage)
