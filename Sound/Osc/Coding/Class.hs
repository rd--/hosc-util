-- | A type-class for Osc encoding/decoding functions.
module Sound.Osc.Coding.Class where

import qualified Data.ByteString as Strict {- bytestring -}
import qualified Data.ByteString.Lazy as Lazy {- bytestring -}

import qualified Sound.Osc.Coding.Decode.Binary as Binary {- hosc -}
import qualified Sound.Osc.Coding.Encode.Builder as Builder {- hosc -}
import Sound.Osc.Packet {- hosc -}

-- | Converting from and to binary packet representations.
class Coding a where
  -- | Decode an OSC packet.
  encodePacket :: PacketOf Message -> a

  -- | Encode an OSC packet.
  decodePacket :: a -> PacketOf Message

instance Coding Strict.ByteString where
  encodePacket = Builder.encodePacket_strict
  decodePacket = Binary.decodePacket_strict

instance Coding Lazy.ByteString where
  encodePacket = Builder.encodePacket
  decodePacket = Binary.decodePacket

{-
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.ByteString.Lazy.Char8 as Char8 {- bytestring -}
instance Coding String where
    encodePacket = Char8.unpack . encodePacket
    decodePacket = decodePacket . Char8.pack
-}

-- | 'encodePacket' of 'Packet_Message'.
encodeMessage :: Coding c => Message -> c
encodeMessage = encodePacket . Packet_Message

-- | 'encodePacket' of 'Packet_Bundle'.
encodeBundle :: Coding c => BundleOf Message -> c
encodeBundle = encodePacket . Packet_Bundle

-- | 'packet_to_message' of 'decodePacket'.
decodeMessage :: Coding c => c -> Maybe Message
decodeMessage = packet_to_message . decodePacket

-- | 'packet_to_bundle' of 'decodePacket'.
decodeBundle :: Coding c => c -> BundleOf Message
decodeBundle = packet_to_bundle . decodePacket
