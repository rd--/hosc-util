-- | Parser for Osc datum.
module Sound.Osc.Datum.Parse where

import Data.Maybe {- base -}

import Sound.Osc.Datum {- hosc -}
import Sound.Osc.Time {- hosc -}

{- | Given 'DatumType' attempt to parse 'Datum' at 'String' using the Haskell reader.
Int32, Int64, Float and Double map straight forwardly.
TimeStamp is read as an Ntp64 integer.
String is correct for ascii input.
Blob is read as a list of integers and is correct for byte input.
Midi is read as a 4-tuple of integers and is correct for byte input.

>>> let parser = parse_datum_haskell
>>> parser 'i' "42" == Just (Int32 42)
True

>>> parser 'h' "42" == Just (Int64 42)
True

>>> parser 'f' "3.14159" == Just (Float 3.14159)
True

>>> parser 'd' "3.14159" == Just (Double 3.14159)
True

>>> parser 't' "429496729600" == Just (TimeStamp {d_timestamp = 100.0})
True

>>> parser 's' "\"pi\"" == Just (string "pi")
True

>>> parser 'b' "[112,105]" == Just (Blob (blob_pack [112,105]))
True

>>> parser 'm' "(0,144,60,90)" == Just (midi (0,144,60,90))
True

>>> parser 's' "\"straße\"" == Just (string "straße") -- unicode
True

>>> parser 'b' "[-1,256]" == Just (Blob (blob_pack [255, 0])) -- non byte
True

>>> parser 'm' "(-1,256,0,255)" == Just (midi (255,0,0,255)) -- non byte
True
-}
parse_datum_haskell :: DatumType -> String -> Maybe Datum
parse_datum_haskell ty =
  let reads_exact s = case reads s of [(r, "")] -> Just r; _ -> Nothing
  in case ty of
      'i' -> fmap Int32 . reads_exact
      'h' -> fmap Int64 . reads_exact
      'f' -> fmap Float . reads_exact
      'd' -> fmap Double . reads_exact
      's' -> fmap (AsciiString . ascii) . reads_exact
      'b' -> fmap (Blob . blob_pack) . reads_exact
      't' -> fmap (TimeStamp . ntpi_to_ntpr) . reads_exact
      'm' -> fmap midi . reads_exact
      _ -> error "parse_datum: unknown type"

-- | Erroring variant of 'parse_datum_haskell'.
parse_datum_haskell_err :: DatumType -> String -> Datum
parse_datum_haskell_err ty = fromMaybe (error "parse_datum_haskell") . parse_datum_haskell ty
