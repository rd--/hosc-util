-- | Pretty printing for Osc datum.
module Sound.Osc.Datum.Pp where

import Data.Char {- base -}
import Data.List {- base -}
import Text.Printf {- base -}

import Sound.Osc.Datum {- hosc -}
import Sound.Osc.Text {- hosc -}

{- | Pretty printer for vectors.

>>> vecPp show [1::Int,2,3]
"<1,2,3>"
-}
vecPp :: (a -> String) -> [a] -> String
vecPp f v = '<' : concat (intersperse "," (map f v)) ++ ">"

{- | Pretty printer for blobs, two-digit zero-padded hexadecimal.

Hugs has no printf instance for Word8 and no %X directive.

>>> blobPp (blob_pack [0, 63, 64, 127, 128, 255])
"B<00,3f,40,7f,80,ff>"
-}
blobPp :: Blob -> String
blobPp = ('B' :) . vecPp (printf "%02x") . blob_unpack_int

-- | Print strings in double quotes iff they contain white space.
stringPp :: String -> String
stringPp x = if any isSpace x then show x else x

{- | Pretty printer for 'Datum'.

>>> let d = [Int32 1,Float 1.2,string "str",midi (0,0x90,0x40,0x60),blob [12,16], TimeStamp 100.0]
>>> map (datumPp (Just 5)) d
["1","1.2","str","M<0,144,64,96>","B<0c,10>","100.0"]
-}
datumPp :: FpPrecision -> Datum -> String
datumPp p d =
  case d of
    Int32 n -> show n
    Int64 n -> show n
    Float n -> showFloatWithPrecision p n
    Double n -> showFloatWithPrecision p n
    AsciiString s -> stringPp (ascii_to_string s)
    Blob s -> blobPp s
    TimeStamp t -> showFloatWithPrecision p t
    Midi (MidiData b1 b2 b3 b4) -> 'M' : vecPp show [b1, b2, b3, b4]

{- | Variant of 'datumPp' that appends the 'datum_type_name'.

>>> map (datum_pp_typed (Just 4)) [Int32 1, Float 2.3, string "4", blob [5, 6], TimeStamp 100.0]
["1:Int32","2.3:Float","4:String","B<05,06>:Blob","100.0:TimeStamp"]
-}
datum_pp_typed :: FpPrecision -> Datum -> String
datum_pp_typed fp d = datumPp fp d ++ ":" ++ snd (datum_type_name d)
