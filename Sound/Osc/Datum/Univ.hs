{-# OPTIONS_GHC -fno-warn-orphans #-}

{- | Functions to allow using the "Sound.OpenSoundControl" 'Datum' as
a /universal/ data type.  In addition to the functions defined
below it provides instances for:

'Datum' are 'IsString'

> :set -XOverloadedStrings
> "string" :: Datum

'Datum' are 'Integral'

>>> toInteger (Int64 12)
12

>>> quotRem 24 7 == (Int64 3,Int64 3)
True

>>> 12 `mod` 7 == Int64 5
True

'Datum' are 'Num'

>>> 5 == Int64 5
True

>>> 5 + 4 == Int64 9
True

>>> negate 5 == Int64 (-5)
True

'Datum' are 'Fractional'

>>> 5.0 == Double 5
True

>>> (5.0 / 4.0) == Double 1.25
True

'Datum' are 'Floating'

>>> pi == 3.141592653589793
True

>>> sqrt (Float 4) == Float 2
True

>>> (2.0 ** 3.0) == Double 8
True

'Datum' are 'Real'

>>> toRational (Double 1.5) == (3/2 :: Rational)
True

>>> (realToFrac (1.5 :: Double) :: Datum) == Double 1.5
True

>>> (realToFrac (Double 1.5) :: Datum) == Double 1.5
True

>>> (realToFrac (Double 1.5) :: Double) == 1.5
True

'Datum' are 'RealFrac'

>>> round (Double 1.4) == Int64 1
True

'Datum' are 'RealFloat'

>>> isNaN (sqrt (negate (Float 1))) == True
True

'Datum' are 'Ord'

>>> 7.5 > 7.0
True

>>> Float 7.5 > Float 7.0
True

>>> string "because" > string "again"
True

'Datum' are 'Enum'

>>> [Int32 0 .. Int32 4] == [Int32 0,Int32 1,Int32 2,Int32 3,Int32 4]
True

>>> [Double 1 .. Double 3] == [Double 1,Double 2,Double 3]
True

'Datum' are 'Random'

> System.Random.randomRIO (Int32 0,Int32 9) :: IO Datum
> System.Random.randomRIO (Float 0,Float 1) :: IO Datum
-}
module Sound.Osc.Datum.Univ where

import qualified Data.ByteString.Char8 as C {- bytestring -}
import Data.Int {- base -}
import Data.Ratio {- base -}
import Data.String {- base -}
import System.Random {- random -}

import Sound.Osc {- hosc -}

-- * Lifting

-- | Unary operator.
type UOp n = (n -> n)

{- | Lift an equivalent set of 'Int32', 'Int64', 'Float' and 'Double' unary
functions to 'Datum'.

>>> map (liftD abs abs abs abs) [Int32 5,Float (-5)] == [Int32 5,Float 5]
True
-}
liftD :: UOp Int32 -> UOp Int64 -> UOp Float -> UOp Double -> UOp Datum
liftD fi fh ff fd d =
  case d of
    Int32 n -> Int32 (fi n)
    Int64 n -> Int64 (fh n)
    Float n -> Float (ff n)
    Double n -> Double (fd n)
    _ -> error "liftD: NaN"

liftD_f :: UOp Float -> UOp Double -> UOp Datum
liftD_f = liftD (error "liftD: Int32?") (error "liftD: Int64?")

-- | A binary operator.
type BinOp n = (n -> n -> n)

{- | Given 'Int32', 'Int64', 'Float' and 'Double' binary operators
generate 'Datum' operator.  If 'Datum' are of equal type result
type is equal, else result type is 'Double'.

>>> liftD2 (+) (+) (+) (+) (Float 1) (Float 2) == Float 3
True

>>> liftD2 (*) (*) (*) (*) (Int32 3) (Float 4) == Double 12
True
-}
liftD2 ::
  BinOp Int32 ->
  BinOp Int64 ->
  BinOp Float ->
  BinOp Double ->
  BinOp Datum
liftD2 fi fh ff fd d1 d2 =
  case (d1, d2) of
    (Int32 n1, Int32 n2) -> Int32 (fi n1 n2)
    (Int64 n1, Int64 n2) -> Int64 (fh n1 n2)
    (Float n1, Float n2) -> Float (ff n1 n2)
    (Double n1, Double n2) -> Double (fd n1 n2)
    _ -> case (datum_floating d1, datum_floating d2) of
      (Just n1, Just n2) -> Double (fd n1 n2)
      _ -> error "liftD2: NaN"

liftD2_f :: BinOp Float -> BinOp Double -> BinOp Datum
liftD2_f = liftD2 (error "liftD2: Int32?") (error "liftD2: Int64?")

-- * At

{- | Direct unary 'Int32', 'Int64', 'Float' and 'Double' functions at
'Datum' fields, or 'error'.

>>> atD show show show show (Int32 5) == "5"
True
-}
atD ::
  (Int32 -> a) ->
  (Int64 -> a) ->
  (Float -> a) ->
  (Double -> a) ->
  Datum ->
  a
atD fi fh ff fd d =
  case d of
    Int32 n -> fi n
    Int64 n -> fh n
    Float n -> ff n
    Double n -> fd n
    _ -> error "atD: NaN"

atD_f :: (Float -> a) -> (Double -> a) -> Datum -> a
atD_f = atD (error "atD: Int32?") (error "atD: Int64?")

-- | Binary /at/ function.
type BinAt n a = (n -> n -> a)

{- | Direct binary 'Int', 'Float' and 'Double' functions at 'Datum'
fields, or 'error'.
-}
atD2 ::
  BinAt Int32 a ->
  BinAt Int64 a ->
  BinAt Float a ->
  BinAt Double a ->
  BinAt Datum a
atD2 fi fh ff fd d1 d2 =
  case (d1, d2) of
    (Int32 n1, Int32 n2) -> fi n1 n2
    (Int64 n1, Int64 n2) -> fh n1 n2
    (Float n1, Float n2) -> ff n1 n2
    (Double n1, Double n2) -> fd n1 n2
    _ -> error "atD2: NaN"

atD2_f :: BinAt Float a -> BinAt Double a -> BinAt Datum a
atD2_f = atD2 (error "atD2: Int32?") (error "atD2: Int64?")

-- | Ternary /at/ function.
type TriAt n a = (n -> n -> n -> a)

{- | Direct ternary 'Int', 'Float' and 'Double' functions at 'Datum'
fields, or 'error'.
-}
atD3 ::
  TriAt Int32 a ->
  TriAt Int64 a ->
  TriAt Float a ->
  TriAt Double a ->
  TriAt Datum a
atD3 fi fh ff fd d1 d2 d3 =
  case (d1, d2, d3) of
    (Int32 n1, Int32 n2, Int32 n3) -> fi n1 n2 n3
    (Int64 n1, Int64 n2, Int64 n3) -> fh n1 n2 n3
    (Float n1, Float n2, Float n3) -> ff n1 n2 n3
    (Double n1, Double n2, Double n3) -> fd n1 n2 n3
    _ -> error "atD3: NaN"

instance IsString Datum where
  fromString = AsciiString . C.pack

instance Integral Datum where
  toInteger d =
    case d of
      Int32 x -> toInteger x
      Int64 x -> toInteger x
      _ -> error "Datum.toInteger"
  quotRem d1 d2 =
    case (d1, d2) of
      (Int32 x, Int32 y) -> let (q, r) = quotRem x y in (Int32 q, Int32 r)
      (Int64 x, Int64 y) -> let (q, r) = quotRem x y in (Int64 q, Int64 r)
      _ -> error "Datum.quotRem"

instance Num Datum where
  negate = liftD negate negate negate negate
  (+) = liftD2 (+) (+) (+) (+)
  (-) = liftD2 (-) (-) (-) (-)
  (*) = liftD2 (*) (*) (*) (*)
  abs = liftD abs abs abs abs
  signum = liftD signum signum signum signum
  fromInteger n = Int64 (fromInteger n)

instance Fractional Datum where
  recip = liftD_f recip recip
  (/) = liftD2_f (/) (/)
  fromRational n = Double (fromRational n)

instance Floating Datum where
  pi = Double pi
  exp = liftD_f exp exp
  log = liftD_f log log
  sqrt = liftD_f sqrt sqrt
  (**) = liftD2_f (**) (**)
  logBase = liftD2_f logBase logBase
  sin = liftD_f sin sin
  cos = liftD_f cos cos
  tan = liftD_f tan tan
  asin = liftD_f asin asin
  acos = liftD_f acos acos
  atan = liftD_f atan atan
  sinh = liftD_f sinh sinh
  cosh = liftD_f cosh cosh
  tanh = liftD_f tanh tanh
  asinh = liftD_f asinh asinh
  acosh = liftD_f acosh acosh
  atanh = liftD_f atanh atanh

instance Real Datum where
  toRational d =
    case d of
      Int32 n -> fromIntegral n % 1
      Int64 n -> fromIntegral n % 1
      Float n -> toRational n
      Double n -> toRational n
      _ -> error "Datum.toRational: NaN"

instance RealFrac Datum where
  properFraction d =
    let (i, j) = properFraction (d_double d)
    in (i, Double j)
  truncate = atD_f truncate truncate
  round = atD_f round round
  ceiling = atD_f ceiling ceiling
  floor = atD_f floor floor

instance RealFloat Datum where
  floatRadix = atD_f floatRadix floatRadix
  floatDigits = atD_f floatDigits floatDigits
  floatRange = atD_f floatRange floatRange
  decodeFloat = atD_f decodeFloat decodeFloat
  encodeFloat i = Double . encodeFloat i
  exponent = atD_f exponent exponent
  significand = liftD_f significand significand
  scaleFloat i = liftD_f (scaleFloat i) (scaleFloat i)
  isNaN = atD_f isNaN isNaN
  isInfinite = atD_f isInfinite isInfinite
  isDenormalized = atD_f isDenormalized isDenormalized
  isNegativeZero = atD_f isNegativeZero isNegativeZero
  isIEEE = atD_f isIEEE isIEEE
  atan2 = liftD2_f atan2 atan2

-- | A comparison that only compares the ordinary kinds with each other.
datumCompareEqualKinds :: Datum -> Datum -> Ordering
datumCompareEqualKinds p q =
  case (p, q) of
    (Int32 i, Int32 j) -> compare i j
    (Int64 i, Int64 j) -> compare i j
    (Float i, Float j) -> compare i j
    (Double i, Double j) -> compare i j
    (AsciiString i, AsciiString j) -> compare i j
    (TimeStamp i, TimeStamp j) -> compare i j
    _ -> error "Datum.compare"

-- Datum at hosc have the plain derived instance
-- instance Ord Datum where compare = datumCompareEqualKinds

instance Enum Datum where
  fromEnum = atD fromEnum fromEnum fromEnum fromEnum
  enumFrom =
    atD
      (map Int32 . enumFrom)
      (map Int64 . enumFrom)
      (map Float . enumFrom)
      (map Double . enumFrom)
  enumFromThen =
    atD2
      (\a -> map Int32 . enumFromThen a)
      (\a -> map Int64 . enumFromThen a)
      (\a -> map Float . enumFromThen a)
      (\a -> map Double . enumFromThen a)
  enumFromTo =
    atD2
      (\a -> map Int32 . enumFromTo a)
      (\a -> map Int64 . enumFromTo a)
      (\a -> map Float . enumFromTo a)
      (\a -> map Double . enumFromTo a)
  enumFromThenTo =
    atD3
      (\a b -> map Int32 . enumFromThenTo a b)
      (\a b -> map Int64 . enumFromThenTo a b)
      (\a b -> map Float . enumFromThenTo a b)
      (\a b -> map Double . enumFromThenTo a b)
  toEnum = Int64 . fromIntegral

instance Random Datum where
  randomR i g =
    case i of
      (Int32 l, Int32 r) -> let (n, g') = randomR (l, r) g in (Int32 n, g')
      (Int64 l, Int64 r) -> let (n, g') = randomR (l, r) g in (Int64 n, g')
      (Float l, Float r) -> let (n, g') = randomR (l, r) g in (Float n, g')
      (Double l, Double r) -> let (n, g') = randomR (l, r) g in (Double n, g')
      _ -> error "Datum.randomR: NaN"
  random g = let (n, g') = randomR (0 :: Double, 1 :: Double) g in (Double n, g')

{-

import Sound.SC3 {- hsc3 -}

'Datum' are 'EqE'

> Int32 5 /=* Int32 6 == Int32 1
> Double 5 ==* Double 5 == Double 1

instance EqE Datum where
    (==*) = liftD2 (==*) (==*) (==*) (==*)
    (/=*) = liftD2 (/=*) (/=*) (/=*) (/=*)

'Datum' are 'OrdE'

> Int32 7 >* Int32 7 == Int32 0
> Double 7.5 >* Int32 7 == Double 1

instance OrdE Datum where
    (>*) = liftD2 (>*) (>*) (>*) (>*)
    (>=*) = liftD2 (>=*) (>=*) (>=*) (>=*)
    (<*) = liftD2 (<*) (<*) (<*) (<*)
    (<=*) = liftD2 (<=*) (<=*) (<=*) (<=*)

'Datum' are 'RealFracE'

> roundE (Double 1.4) == Double 1
> ceilingE (Double 1.4) == Double 2

instance RealFracE Datum where
  truncateE = liftD undefined undefined truncateE truncateE
  roundE = liftD undefined undefined roundE roundE
  ceilingE = liftD undefined undefined ceilingE ceilingE
  floorE = liftD undefined undefined floorE floorE

-}
