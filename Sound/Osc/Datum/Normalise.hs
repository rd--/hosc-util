-- | Datum normalisation.
module Sound.Osc.Datum.Normalise where

import Sound.Osc.Datum {- hosc -}
import Sound.Osc.Packet as Osc {- hosc -}

{- | Lift 'Osc.Int32' to 'Osc.Int64' and 'Osc.Float' to 'Osc.Double'.

>>> map normalise_datum [Int32 1,Float 1] == [Int64 1,Double 1]
True
-}
normalise_datum :: Datum -> Datum
normalise_datum d =
  case d of
    Int32 n -> Int64 (fromIntegral n)
    Float n -> Double (realToFrac n)
    _ -> d

{- | A normalised 'Osc.Message' has only 'Osc.Int64' and 'Osc.Double' numerical values.

>>> normalise_message (message "/m" [Int32 0,Float 0]) == message "/m" [Int64 0,Double 0]
True
-}
normalise_message :: Message -> Message
normalise_message = message_coerce normalise_datum

-- | A normalised 'Osc.Bundle' has only 'Osc.Int64' and 'Osc.Double' numerical values.
normalise_bundle :: BundleOf Message -> BundleOf Message
normalise_bundle = bundle_coerce normalise_datum

-- * Coercion

-- | Map a normalising function over datum at an Osc 'Message'.
message_coerce :: (Datum -> Datum) -> Message -> Message
message_coerce f (Message s xs) = Message s (map f xs)

-- | Map a normalising function over datum at an Osc 'Bundle'.
bundle_coerce :: (Datum -> Datum) -> BundleOf Message -> BundleOf Message
bundle_coerce f (Bundle t xs) = Bundle t (map (message_coerce f) xs)

-- * Promotion

{- | Coerce 'Int32', 'Int64' and 'Float' to 'Double'.

>>> map datum_promote [Int32 5,Float 5] == [Double 5,Double 5]
True
-}
datum_promote :: Datum -> Datum
datum_promote d =
  case d of
    Int32 n -> Double (fromIntegral n)
    Int64 n -> Double (fromIntegral n)
    Float n -> Double (realToFrac n)
    _ -> d

{- | 'Osc.Datum' as 'Osc.Int64' if 'Osc.Int32', 'Osc.Int64', 'Osc.Float' or
'Osc.Double'.

>>> let d = [Int32 5,Int64 5,Float 5.5,Double 5.5,string "5"]
>>> map datum_floor d == [Int64 5,Int64 5,Int64 5,Int64 5,string "5"]
True
-}
datum_floor :: Datum -> Datum
datum_floor d =
  case d of
    Int32 x -> Int64 (fromIntegral x)
    Float x -> Int64 (fromInteger (floor x))
    Double x -> Int64 (fromInteger (floor x))
    _ -> d
