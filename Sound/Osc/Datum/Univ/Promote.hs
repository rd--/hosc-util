-- | Functions to promote (lift) functions to 'Datum'.
module Sound.Osc.Datum.Univ.Promote where

import Sound.Osc {- hosc -}

import Sound.Osc.Datum.Normalise {- hosc-utils -}
import Sound.Osc.Datum.Univ {- hosc-utils -}

{- | Lift a 'Double' unary operator to 'Datum' via 'datum_promote'.

>>> liftD_promote negate (Int32 5) == Double (-5)
True
-}
liftD_promote :: UOp Double -> UOp Datum
liftD_promote fd =
  liftD (error "liftD_promote") (error "liftD_promote") (error "liftD_promote") fd
    . datum_promote

{- | A 'datum_promote' variant of 'liftD2'.

>>> liftD2_promote (+) (Float 1) (Float 2) == Double 3
True
-}
liftD2_promote :: BinOp Double -> BinOp Datum
liftD2_promote f d1 d2 =
  liftD2
    (error "liftD2_promote")
    (error "liftD2_promote")
    (error "liftD2_promote")
    f
    (datum_promote d1)
    (datum_promote d2)

{- | Lift a 'Double' /at/ operator to 'Datum' via 'datum_promote'.

>>> atD_promote floatRadix (Int32 5) == 2
True
-}
atD_promote :: (Double -> a) -> Datum -> a
atD_promote f = f . d_double . datum_promote
