{-# LANGUAGE FlexibleInstances #-}

-- | Typeclass for encoding and decoding Osc packets.
module Sound.Osc.Packet.Class where

import Sound.Osc.Packet {- hosc -}

import Sound.Osc.Coding.Class {- hosc-util -}

-- | A type-class for values that can be translated to and from Osc 'Packet's.
class Osc o where
  toPacket ::
    o ->
    -- | Translation to 'Packet'.
    PacketOf Message
  fromPacket ::
    PacketOf Message ->
    -- | Translation from 'Packet'.
    Maybe o

instance Osc Message where
  toPacket = Packet_Message
  fromPacket = packet_to_message

instance Osc (BundleOf Message) where
  toPacket = Packet_Bundle
  fromPacket = Just . packet_to_bundle

instance Osc (PacketOf Message) where
  toPacket = id
  fromPacket = Just

-- | 'encodePacket' '.' 'toPacket'.
encodeOsc :: (Coding c, Osc o) => o -> c
encodeOsc = encodePacket . toPacket

-- | 'fromPacket' '.' 'decodePacket'.
decodeOsc :: (Coding c, Osc o) => c -> Maybe o
decodeOsc = fromPacket . decodePacket
