-- | Packet coercion.
module Sound.Osc.Packet.Coerce where

import Sound.Osc.Datum {- hosc -}
import Sound.Osc.Packet {- hosc -}

-- | Map a normalising function over datum at an Osc 'Message'.
message_coerce :: (Datum -> Datum) -> Message -> Message
message_coerce f (Message s xs) = Message s (map f xs)

-- | Map a normalising function over datum at an Osc 'Bundle'.
bundle_coerce :: (Datum -> Datum) -> BundleOf Message -> BundleOf Message
bundle_coerce f (Bundle t xs) = Bundle t (map (message_coerce f) xs)
