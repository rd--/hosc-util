-- | Pretty printing for Osc packets
module Sound.Osc.Packet.Pp where

import Data.List {- base -}

import Sound.Osc.Packet {- hosc -}
import Sound.Osc.Text {- hosc -}

import Sound.Osc.Datum.Pp {- hosc-util -}

{- | Pretty printer for 'Message'.

> messagePp Nothing (Message "/m" [int32 0,float 1.0,string "s",midi (1,2,3,4),blob [1,2,3]])
-}
messagePp :: FpPrecision -> Message -> String
messagePp p (Message a d) = let d' = map (datumPp p) d in unwords (a : d')

-- | Pretty printer for 'Bundle'.
bundlePp :: FpPrecision -> BundleOf Message -> String
bundlePp p (Bundle t m) = let m' = intersperse ";" (map (messagePp p) m) in unwords (showFloatWithPrecision p t : m')

-- | Pretty printer for 'Packet'.
packetPp :: FpPrecision -> PacketOf Message -> String
packetPp p pkt =
  case pkt of
    Packet_Message m -> messagePp p m
    Packet_Bundle b -> bundlePp p b
